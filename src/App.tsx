import React, { Fragment } from 'react';
import { 
  BrowserRouter as Router,
  Routes,
  Route } from 'react-router-dom';
import LoginPage from './components/LoginPage';
import AdminPage from './components/AdminPage';
import CustomerPage from './components/CustomerPage';
import DeliveryParnterPage from './components/DeliveryPartnerPage';

const App: React.FC = () => {

  const isAuthenticated = !!localStorage.getItem('token');
  const userType = localStorage.getItem('userType');

  return (
    <Router>
        {isAuthenticated ? (
          <Routes>
            <Route path='/login' element={<LoginPage />}/>
            <Route path='/customer' element={<CustomerPage />}/>
            <Route path='/delivery-partner' element={<DeliveryParnterPage />}/>
            <Route path='/admin' element={<AdminPage />}/>
            <Route path='/' element={<AdminPage />}/>
          </Routes>
        ) : (
            <Routes>
              <Route path='*' element={<LoginPage />}/>
              <Route path='/admin' element={<AdminPage />}/>
            </Routes>
        )}
        {userType === 'admin'?
                  <AdminPage/> : <></>}
    </Router>
  );
};

export default App;
