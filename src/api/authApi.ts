import axios from 'axios';

const BASE_URL = 'http://localhost:8080/api';

export const adminLogin = async (formData: any) => {
  try {
    const response = await axios.post(`${BASE_URL}/admin/login`, formData);
    return response.data;
  } catch (error) {
    throw new Error('Invalid username or password');
  }
};

export const deliveryPartnerLogin = async (formData: any) => {
    try {
      const response = await axios.post(`${BASE_URL}/delivery-partners/login`, formData);
      return response.data;
    } catch (error) {
      throw new Error('Invalid username or password');
    }
};

export const customerLogin = async (formData: any) => {
  try {
    const response = await axios.post(`${BASE_URL}/customers/login`, formData);
    return response.data;
  } catch (error) {
    throw new Error('Invalid username or password');
  }
};