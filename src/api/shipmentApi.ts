import axios from 'axios';

const BASE_URL = 'http://localhost:8080/api/shipments';

export const getDeliveryPartnerShipments = async () => {
  const config = {
    headers: {
      authorization: `Bearer ${localStorage.getItem('token')}`
    }
  };
  try {
    const response = await axios.get(`${BASE_URL}/`, config);
    return response.data;
  } catch (error: any) {
    throw error;
  }
};

export const createShipment = async (formData: any) => {
    const config = {
      headers: {
        authorization: `Bearer ${localStorage.getItem('token')}`
      }
    };
    try {
      const response = await axios.post(`${BASE_URL}/create`, formData, config);
      return response.data;
    } catch (error: any) {
      if(error.code === "ERR_BAD_REQUEST") {
        throw new Error(error?.response?.data?.message || error.message)
      }
      throw error;
    }
};

export const deleteShipment = async (shipmentId: any) => {
  const config = {
    headers: {
      authorization: `Bearer ${localStorage.getItem('token')}`
    }
  };
  try {
    const response = await axios.delete(`${BASE_URL}/${shipmentId}`, config);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const updateShipmentStatus =async (shipmentId:any, status: string) => {
  const config = {
    headers: {
      authorization: `Bearer ${localStorage.getItem('token')}`
    }
  };
  try {
    const response = await axios.put(`${BASE_URL}/${shipmentId}`, {status}, config);
    return response.data;
  } catch (error) {
    throw error;
  }
}