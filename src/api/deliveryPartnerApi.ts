import axios from 'axios';

const BASE_URL = 'http://localhost:8080/api/delivery-partners';

export const onboardDeliveryPartner = async (formData: any) => {
  const config = {
    headers: {
      authorization: `Bearer ${localStorage.getItem('token')}`
    }
  };
  try {
    const response = await axios.post(`${BASE_URL}/create`, formData, config);
    return response.data;
  } catch (error: any) {
    if(error.code === "ERR_BAD_REQUEST") {
      throw new Error(error?.response?.data?.message || error.message)
    }
    throw error;
  }
};

export const offboardDeliveryPartner = async (customerId: any) => {
  const config = {
    headers: {
      authorization: `Bearer ${localStorage.getItem('token')}`
    }
  };
  try {
    const response = await axios.delete(`${BASE_URL}/${customerId}`, config);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const getDeliveryPartnerList = async () => {
  const config = {
    headers: {
      authorization: `Bearer ${localStorage.getItem('token')}`
    }
  };
  try {
    const response = await axios.get(`${BASE_URL}/`, config);
    return response.data;
  } catch (error) {
    throw error;
  }
}