import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import {
    Form,
    Button,
    Card,
    Container,
    Alert
} from 'react-bootstrap';
import { adminLogin, customerLogin, deliveryPartnerLogin } from '../api/authApi';
import CardHeader from 'react-bootstrap/esm/CardHeader';

const LoginPage: React.FC = () => {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({ email: '', password: '' });
  const [userType, setUserType] = useState('admin');
  const [error, setError] = useState('');

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.id]: e.target.value,
    });
    setError('');
  };
  const handleUserTypeChange = (e: any) => {
    setUserType(e.target.value);
    setError('');
  };

  const handleAdminSubmit = async () => {
    try {
      const response = await adminLogin(formData);
      // Assuming the backend returns a token upon successful login
      localStorage.setItem('token', response.token);
      localStorage.setItem('userType', userType)
      navigate('/admin'); // Redirect to the admin page after login
      setError('');
    } catch (error: any) {
      setError(error.message);
    }
  };

  const handleCustomerSubmit = async () => {
    try {
      const response = await customerLogin(formData);
      // Assuming the backend returns a token upon successful login
      localStorage.setItem('token', response.token);
      localStorage.setItem('userType', userType)
      navigate('/customer');
      setError('');
    } catch (error: any) {
      setError(error.message);
    }
  };

  const handleDeliveryPartnerSubmit = async () => {
    try {
      const response = await deliveryPartnerLogin(formData);
      // Assuming the backend returns a token upon successful login
      localStorage.setItem('token', response.token);
      localStorage.setItem('userType', userType)
      navigate('/delivery-partner');
      setError('');
    } catch (error: any) {
      setError(error.message);
    }
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if(userType === 'customer') {
      handleCustomerSubmit();
    } else if(userType === 'delivery-partner') {
      handleDeliveryPartnerSubmit();
    } else if (userType === 'admin'){
      handleAdminSubmit();
    }
    setError('');
  }

  return (
    <Container className="d-flex justify-content-center align-items-center" style={{ minHeight: '100vh' }}>
        <Card>
            <CardHeader style={{ alignContent: 'center'}}>
                <h3>Login</h3>
            </CardHeader>
            <Card.Body>
              <Form onSubmit={handleSubmit}>
                  <Form.Group className="mb-3" controlId="email">
                      <Form.Label>User Type</Form.Label>
                      <Form.Select onChange={handleUserTypeChange}>
                        <option value='admin'>Admin</option>
                        <option value='customer'>Customer</option>
                        <option value='delivery-partner'>Delivery Partner</option>
                      </Form.Select>
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="email">
                      <Form.Label>Email address</Form.Label>
                      <Form.Control type="email" placeholder="Enter email" onChange={handleChange}/>
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="password">
                      <Form.Label>Password</Form.Label>
                      <Form.Control type="password" placeholder="Password" onChange={handleChange}/>
                  </Form.Group>
                  <Button variant="primary" type="submit">
                      Submit
                  </Button>
              </Form>
            </Card.Body>
            <Card.Footer>
              {error !== ''? 
                <Alert variant='danger'>
                  <p>{error}</p>
                </Alert> : <></>}
            </Card.Footer>
        </Card>
    </Container>
  );
};

export default LoginPage;
