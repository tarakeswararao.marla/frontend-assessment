import React, { Fragment, useState } from "react";
import { Button, Col, Container, Nav, Row } from "react-bootstrap";
import { Route, Routes, useNavigate } from "react-router-dom";
import CustomerList from "./CustomerList";
import DeliveryPartnerList from "./DeliveryPartnerList";
import LoginPage from "./LoginPage";

const AdminPage: React.FC = () => {
    const navigate = useNavigate();

    const logoutAdmin = () => {
        localStorage.clear();
        navigate('/login');
    };
    return (
        <Container fluid>
            <Routes>
                <Route path='/login' element={<LoginPage />}/>
            </Routes>
            <Row>
              <Col sm={3} className='sidebar'>
                <Nav defaultActiveKey="/customer-list" className="flex-column">
                  <Nav.Item>
                    <Nav.Link href="/admin/customer-list">Customers</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link href="/admin/delivery-partner-list">Delivery Partners</Nav.Link>
                  </Nav.Item>
                  <Button className="m-3" onClick={logoutAdmin}>Logout</Button>
                </Nav>
              </Col>
              <Col sm={9}>
                <Routes>
                  <Fragment>
                        <Route path='/admin/customer-list' element={<CustomerList/>}/>
                        <Route path='/admin/delivery-partner-list' element={<DeliveryPartnerList/>}/>
                  </Fragment>
                </Routes>
              </Col>
            </Row>
          </Container>
    );
};

export default AdminPage;