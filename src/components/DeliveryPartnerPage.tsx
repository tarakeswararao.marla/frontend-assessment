import { CDBCard, CDBCardBody, CDBContainer, CDBDataTable } from "cdbreact";
import React, { useEffect, useState } from "react";
import { getDeliveryPartnerShipments, updateShipmentStatus } from "../api/shipmentApi";
import { Alert, Button, Form, Modal } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { getCustomerList } from "../api/customerApi";

const DeliveryParnterPage: React.FC = () => {
    const navigate = useNavigate();
    const [error, setError] = useState('');
    const [shipments, setShipments] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [deliveryStatus, setDeliveryStatus] = useState('pending');
    const [shipmentId, setShipmentId] = useState('');

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const fetchShipments = async () => {
        try {
          const response = await getDeliveryPartnerShipments();
          setShipments(response.shipments);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const fetchCustomers = async () => {
        try {
            const response = await getCustomerList();
            setCustomers(response.customers);
          } catch (error) {
              console.error('Error fetching data:', error);
          }
    };

    useEffect(() => {
        fetchShipments();
        fetchCustomers();
        return () => {};
    }, []);

    const handleChange = (e: any) => {
        setDeliveryStatus(e.target.value);
        setError('');
    };

    const handleUpdateShipment = async (e: any) => {
        e.preventDefault();
        try {
            await updateShipmentStatus(shipmentId, deliveryStatus);
            fetchShipments();
            handleClose();
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const logoutAdmin = () => {
        localStorage.clear();
        navigate('/login');
    };

    const data = () => {
        return {
            columns: [
                {
                    label: 'Name',
                    field: 'name',
                    width: 83,
                    attributes: {
                        'aria-controls': 'DataTable',
                        'aria-label': 'Name',
                    },
                },
                {
                    label: 'Customer',
                    field: 'customer',
                    width: 83
                },
                {
                    label: 'Source Location',
                    field: 'sourceLocation',
                    width: 168
                },
                {
                    label: 'targetLocation',
                    field: 'targetLocation',
                    width: 60
                },
                {
                    label: 'Delivery Status',
                    field: 'status',
                    width: 60
                },
                {
                    label: 'Action',
                    field: 'action',
                    width: 100
                }
            ],
            rows: shipments.map((data: any, idx: any) => {
                data.name = `Shipment-${idx+1}`;
                const customer:any = customers.find((dp:any) => dp._id == data.customerId);
                data.customer = customer ? customer.email : data.customerId;
                data.action = (
                    <Button onClick={(e: any) => {
                        e.preventDefault();
                        handleShow();
                        setShipmentId(data._id);
                    }}>Update Status</Button>)
                return data;
            })
        };
    };

    return (
        <CDBContainer>
            <h2 className="text-center m-2">Delivery Partner</h2>
            <div className="d-flex justify-content-end align-items-end">
                <Button className="m-3" onClick={logoutAdmin}>Logout</Button>
            </div>
            <CDBCard>
                <CDBCardBody>
                    <CDBDataTable
                        striped
                        bordered
                        hover
                        scrollX
                        maxHeight="200vh"
                        materialSearch
                        data={data()}
                    />
                </CDBCardBody>
            </CDBCard>
            <Modal show={show} onHide={handleClose} centered>
                <Form onSubmit={handleUpdateShipment}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add Shipment</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            <Form.Group className="mb-3" controlId="deliveryStatus">
                                <Form.Label>Delivery Partner</Form.Label>
                                <Form.Select onChange={handleChange}>
                                    <option value=''>Select Delivery Partner</option>
                                    <option value='pending'>Pending</option>
                                    <option value='in_progress'>In Progress</option>
                                    <option value='delivered'>Delivered</option>
                                </Form.Select>
                            </Form.Group>
                            {error !== ''? 
                                <Alert variant='danger'>
                                <p>{error}</p>
                                </Alert> : <></>}
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="primary" type="submit">
                                Save
                            </Button>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Form>
            </Modal>
        </CDBContainer>
    );
};

export default DeliveryParnterPage;