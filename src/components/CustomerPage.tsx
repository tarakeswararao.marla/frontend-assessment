import { CDBCard, CDBCardBody, CDBContainer, CDBDataTable } from "cdbreact";
import React, { useEffect, useState } from "react";
import { createShipment, deleteShipment, getDeliveryPartnerShipments } from "../api/shipmentApi";
import { Alert, Button, Form, Modal } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { getDeliveryPartnerList } from "../api/deliveryPartnerApi";

const CustomerPage: React.FC = () => {
    const navigate = useNavigate();
    const [error, setError] = useState('');
    const [shipments, setShipments] = useState([]);
    const [deliveryPartners, setDeliveryPartners] = useState([]);
    const [formData, setFormData] = useState({ 
        sourceLocation: '', 
        targetLocation: '',
        deliveryPartnerId: ''
    });
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const fetchShipments = async () => {
        try {
          const response = await getDeliveryPartnerShipments();
          setShipments(response.shipments);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const fetchDeliveryPartners = async () => {
        try {
          const response = await getDeliveryPartnerList();
          setDeliveryPartners(response.deliveryPartners);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchShipments();
        fetchDeliveryPartners();
        return () => {};
    }, []);

    const logoutAdmin = () => {
        localStorage.clear();
        navigate('/login');
    };

    const handleChange = (e: any) => {
        setFormData({
          ...formData,
          [e.target.id]: e.target.value,
        });
        setError('');
    };

    const handleCreateShipment = async (e: React.FormEvent) => {
        e.preventDefault();
        try {
            await createShipment(formData);
            fetchShipments();
            handleClose();
            setError('');
        } catch (error: any) {
            setError(error.message);
        }
    };

    const handleRemoveShipment = async (shipmentId: string) => {
        try {
            await deleteShipment(shipmentId);
            fetchShipments();
            handleClose();
        } catch (error: any) {
            setError(error.message);
        }
    }

    const data = () => {
        return {
            columns: [
                {
                    label: 'Name',
                    field: 'name',
                    width: 100
                },
                {
                    label: 'Delivery Partner',
                    field: 'deliveryPartner',
                    width: 240
                },
                {
                    label: 'Source Location',
                    field: 'sourceLocation',
                    window: 240 
                },
                {
                    label: 'targetLocation',
                    field: 'targetLocation'
                },
                {
                    label: 'Delivery Status',
                    field: 'status'
                },
                {
                    label: 'Action',
                    field: 'action',
                    width: 300
                }
            ],
            rows: shipments.map((data: any, idx: any) => {
                data.name = `Shipment-${idx+1}`;
                const deliveryPartner:any = deliveryPartners.find((dp:any) => dp._id == data.deliveryPartnerId);
                data.deliveryPartner = deliveryPartner ? deliveryPartner.email : data.deliveryPartnerId;
                data.action = (
                    <div className="d-flex justify-content-center align-items-center">
                        <Button className="m-1" onClick={(e) => {
                            e.preventDefault();
                            handleRemoveShipment(data._id);
                        }}>Remove</Button>
                    </div>
                )
                return data;
            })
        };
    };

    return (
        <CDBContainer>
            <h2 className="text-center m-2">Customer</h2>
            <div className="d-flex justify-content-end align-items-end">
                <Button className="m-3" variant="primary" onClick={handleShow}>Create Shipment</Button>
                <Button className="m-3" onClick={logoutAdmin}>Logout</Button>
            </div>
            <CDBCard>
                <CDBCardBody>
                    <CDBDataTable
                        striped
                        bordered
                        hover
                        scrollX
                        maxHeight="200vh"
                        materialSearch
                        data={data()}
                    />
                </CDBCardBody>
            </CDBCard>
            <Modal show={show} onHide={handleClose} centered>
                <Form onSubmit={handleCreateShipment}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add Shipment</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            <Form.Group className="mb-3" controlId="sourceLocation">
                                <Form.Label>Source Location</Form.Label>
                                <Form.Control type="text" onChange={handleChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="targetLocation">
                                <Form.Label>Target Location</Form.Label>
                                <Form.Control type="text" onChange={handleChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="deliveryPartnerId">
                                <Form.Label>Delivery Partner</Form.Label>
                                <Form.Select onChange={handleChange}>
                                    <option value=''>Select Delivery Partner</option>
                                    {deliveryPartners.map((data:any) => {
                                        return (<option key={data._id} value={data._id}>{data.name}</option>);
                                    })}
                                </Form.Select>
                            </Form.Group>
                            {error !== ''? 
                                <Alert variant='danger'>
                                <p>{error}</p>
                                </Alert> : <></>}
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="primary" type="submit">
                                Save
                            </Button>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Form>
            </Modal>
        </CDBContainer>
    );
};

export default CustomerPage;