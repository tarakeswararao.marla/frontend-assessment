import React, { useEffect, useState } from "react";
import { CDBCard, CDBCardBody, CDBContainer, CDBDataTable } from 'cdbreact';
import { 
    getDeliveryPartnerList,
    onboardDeliveryPartner,
    offboardDeliveryPartner
} from "../api/deliveryPartnerApi";
import { Alert, Button, Form, Modal, Table } from "react-bootstrap";

const DeliveryPartnerList: React.FC = () => {
    const [deliveryPartners, setDeliveryPartners] = useState([]);
    const [show, setShow] = useState(false);
    const [removeModel, setRemoveModel] = useState(false);
    const [deliveryPartnerIdx, setDeliveryPartnerIdx] = useState(0);
    const [formData, setFormData] = useState({ name: '', email: '', phoneNumber: '' });
    const [error, setError] = useState('');

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleRemoveModel = () => setRemoveModel(false);

    const fetchDeliveryPartners = async () => {
        try {
          const response = await getDeliveryPartnerList();
          setDeliveryPartners(response.deliveryPartners);
        } catch (error) {
            console.error('Error fetching data:', error);
            return [];
        }
    };

    useEffect(() => {
        fetchDeliveryPartners();
        return () => {};
    }, []);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFormData({
          ...formData,
          [e.target.id]: e.target.value,
        });
        setError('');
    };

    const handleAddDeliveryPartner = async (e: React.FormEvent) => {
        e.preventDefault();
        try {
            await onboardDeliveryPartner(formData);
            fetchDeliveryPartners();
            setShow(false);
        } catch (error: any) {
            setError(error.message);
        }
    };

    const handleRemoveDeliveryPartner = async (e: React.FormEvent) => {
        e.preventDefault();
        try {
            await offboardDeliveryPartner(deliveryPartners[deliveryPartnerIdx]['_id']);
            fetchDeliveryPartners();
            setRemoveModel(false);
        } catch (error) {
        }
    }

    const data = () => {
        return {
            columns: [
                {
                    label: 'Name',
                    field: 'name',
                    width: 83
                },
                {
                    label: 'Email',
                    field: 'email',
                    width: 168
                },
                {
                    label: 'PhoneNumber',
                    field: 'phoneNumber',
                    width: 60
                },
                {
                    label: 'Password',
                    field: 'textPassword',
                    width: 80
                },
                {
                    label: 'Action',
                    field: 'action',
                    width: 100
                }
            ],
            rows: deliveryPartners.map((data: any, idx: any) => {
                data.action = (<Button onClick={() => {
                    setRemoveModel(true);
                    setDeliveryPartnerIdx(idx);
                }}>remove</Button>)
                return data;
            })
        };
    };
    return (
        <>
            <h2 className="m-2">Delivery Partner</h2>
            <div className="d-flex justify-content-end align-items-end m-3">
                <Button variant="primary" onClick={handleShow}>Create Delivery Partner</Button>
            </div>
            <CDBContainer>
                <CDBCard>
                    <CDBCardBody>
                        <CDBDataTable
                            striped
                            bordered
                            hover
                            scrollX
                            maxHeight="200vh"
                            materialSearch
                            data={data()}
                        />
                    </CDBCardBody>
                </CDBCard>
            </CDBContainer>

            <Modal show={show} onHide={handleClose} centered>
                <Form onSubmit={handleAddDeliveryPartner}>
                    <Modal.Header closeButton>
                        <Modal.Title>OnBoard Delivery Partner</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" onChange={handleChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" onChange={handleChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="phoneNumber">
                                <Form.Label>PhoneNumber</Form.Label>
                                <Form.Control type="text" onChange={handleChange}/>
                            </Form.Group>
                            {error !== ''? 
                                <Alert variant='danger'>
                                <p>{error}</p>
                                </Alert> : <></>}
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                            <Button variant="primary" type="submit">
                                Save
                            </Button>
                        </Modal.Footer>
                    </Form>
            </Modal>

            <Modal show={removeModel} onHide={handleRemoveModel} centered>
                <Modal.Header closeButton>
                    Delivery Partner Details
                </Modal.Header>
                <Modal.Body>
                    <Table responsive>
                        <thead>
                            <tr>
                                <th>Name:</th>
                                <th>Email:</th>
                                <th>Phone Number:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{deliveryPartners[deliveryPartnerIdx] ? deliveryPartners[deliveryPartnerIdx]['name'] : ''}</td>
                                <td>{deliveryPartners[deliveryPartnerIdx] ? deliveryPartners[deliveryPartnerIdx]['email'] : ''}</td>
                                <td>{deliveryPartners[deliveryPartnerIdx] ? deliveryPartners[deliveryPartnerIdx]['phoneNumber'] : ''}</td>
                            </tr>
                        </tbody>
                    </Table>
                    
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" className="m-2 text-center" onClick={handleRemoveDeliveryPartner}>
                        Remove
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};

export default DeliveryPartnerList;