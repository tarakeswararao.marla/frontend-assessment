import React, { useEffect, useState } from "react";
import { CDBCard, CDBCardBody, CDBContainer, CDBDataTable } from 'cdbreact';
import { getCustomerList, onboardCustomer, offboardCustomer } from "../api/customerApi";
import { Alert, Button, Form, Modal, Table } from "react-bootstrap";

const CustomerList: React.FC = () => {
    const [customers, setCustomers] = useState([]);
    const [show, setShow] = useState(false);
    const [removeModel, setRemoveModel] = useState(false);
    const [customerId, setCustomerId] = useState(0);
    const [formData, setFormData] = useState({ name: '', email: '', phoneNumber: '' });
    const [error, setError] = useState('');

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleRemoveModel = () => setRemoveModel(false);

    const fetchCustomers = async () => {
        try {
          const response = await getCustomerList();
          setCustomers(response.customers);
        } catch (error) {
            console.error('Error fetching data:', error);
            return [];
        }
    };

    useEffect(() => {
        fetchCustomers();
        return () => {};
    }, []);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFormData({
          ...formData,
          [e.target.id]: e.target.value,
        });
        setError('');
    };

    const handleAddCustomer = async (e: React.FormEvent) => {
        e.preventDefault();
        try {
            await onboardCustomer(formData);
            fetchCustomers();
            setShow(false);
        } catch (error: any) {
            setError(error.message);
        }
    };

    const handleRemoveCustomer = async (e: React.FormEvent) => {
        e.preventDefault();
        try {
            await offboardCustomer(customers[customerId]['_id']);
            fetchCustomers();
            setRemoveModel(false);
        } catch (error) {
        }
    }

    const data = () => {
        return {
            columns: [
                {
                    label: 'Name',
                    field: 'name',
                    width: 88
                },
                {
                    label: 'Email',
                    field: 'email',
                    width: 176
                },
                {
                    label: 'PhoneNumber',
                    field: 'phoneNumber',
                    width: 50
                },
                {
                    label: 'Password',
                    field: 'textPassword',
                    width: 80
                },
                {
                    label: 'Action',
                    field: 'action',
                    width: 100
                }
            ],
            rows: customers.map((data: any, idx: any) => {
                data.action = (<Button onClick={() => {
                    setRemoveModel(true);
                    setCustomerId(idx);
                }}>remove</Button>)
                return data;
            })
        };
    };
    return (
        <>
            <h2 className="m-2">Customers</h2>
            <div className="d-flex justify-content-end align-items-end m-3">
                <Button variant="primary" onClick={handleShow}>Add Customer</Button>
            </div>
            <CDBContainer>
                <CDBCard>
                    <CDBCardBody>
                        <CDBDataTable
                            striped
                            bordered
                            hover
                            scrollX
                            maxHeight="200vh"
                            materialSearch
                            data={data()}
                        />
                    </CDBCardBody>
                </CDBCard>
            </CDBContainer>

            <Modal show={show} onHide={handleClose} centered>
                <Form onSubmit={handleAddCustomer}>
                    <Modal.Header closeButton>
                        <Modal.Title>OnBoard Customer</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" onChange={handleChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" onChange={handleChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="phoneNumber">
                                <Form.Label>PhoneNumber</Form.Label>
                                <Form.Control type="text" onChange={handleChange}/>
                            </Form.Group>
                            {error !== ''? 
                                <Alert variant='danger'>
                                <p>{error}</p>
                                </Alert> : <></>}
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="primary" type="submit">
                                Save
                            </Button>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Form>
            </Modal>

            <Modal show={removeModel} onHide={handleRemoveModel} centered>
                <Modal.Header closeButton>
                    Customer Details
                </Modal.Header>
                <Modal.Body>
                    <Table responsive>
                        <thead>
                            <tr>
                                <th>Name:</th>
                                <th>Email:</th>
                                <th>Phone Number:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{customers[customerId] ? customers[customerId]['name'] : ''}</td>
                                <td>{customers[customerId] ? customers[customerId]['email'] : ''}</td>
                                <td>{customers[customerId] ? customers[customerId]['phoneNumber'] : ''}</td>
                            </tr>
                        </tbody>
                    </Table>
                    
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type="submit" className="m-2 text-center" onClick={handleRemoveCustomer}>
                        Remove
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};

export default CustomerList;